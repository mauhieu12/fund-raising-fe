export class Group {

  id?: number
  groupName?: string
  description?: string
  groupOwner?: number
  groupCoOwner?: number
  joinCode?: string
}
