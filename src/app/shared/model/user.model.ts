export class User {
  id?: number
  password?: string
  firstName?: string
  lastName?: string
  phoneNumber?: string
  email?: string
}
