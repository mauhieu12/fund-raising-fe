import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-create-group-success-dialog',
  templateUrl: './create-group-success-dialog.component.html',
  styleUrls: ['./create-group-success-dialog.component.scss']
})
export class CreateGroupSuccessDialogComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
