import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-join-group-successful-dialog',
  templateUrl: './join-group-successful-dialog.component.html',
  styleUrls: ['./join-group-successful-dialog.component.scss']
})
export class JoinGroupSuccessfulDialogComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
